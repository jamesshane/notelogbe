const express = require('express');
const connectdatabase = require('./database/Database');
var cors = require('cors');
const notes = require('./api/Notes');
const linkrefs = require('./api/Linkrefs');
const app = express();

app.get('/', (req, res) => res.send('NoteLogBE'));

connectdatabase();

// cors
app.use(cors({ origin: true, credentials: true }));;

// Init Middleware
app.use(express.json({ extended: false }));

app.use('/notes', notes);
app.use('/linkrefs', linkrefs);

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));