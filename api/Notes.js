const express = require('express');
const router = express.Router();

// Load Note model
const Note = require('../database/models/Note');

// @route GET api/notes/test
// @description tests notes route
// @access Public
router.get('/test', (req, res) => res.send('route testing!'));

// @route GET api/notes
// @description Get all notes
// @access Public
router.get('/', (req, res) => {
  Note.find()
    .then(notes => res.json(notes))
    .catch(err => res.status(404).json({ nonotesfound: 'No Notes found' }));
});

// @route GET api/notes/:id
// @description Get single note by id
// @access Public
router.get('/:id', (req, res) => {
  Note.findById(req.params.id)
    .then(note => res.json(note))
    .catch(err => res.status(404).json({ nonotefound: 'No Note found' }));
});

// @route GET api/notes
// @description add/save note
// @access Public
router.post('/', (req, res) => {
  Note.create(req.body)
    .then(note => res.json({ msg: 'Note added successfully' }))
    .catch(err => res.status(400).json({ error: 'Unable to add this note' }));
});

// @route GET api/notes/:id
// @description Update note
// @access Public
router.put('/:id', (req, res) => {
  Note.findByIdAndUpdate(req.params.id, req.body)
    .then(note => res.json({ msg: 'Updated successfully' }))
    .catch(err =>
      res.status(400).json({ error: 'Unable to update the Database' })
    );
});

// @route GET api/notes/:id
// @description Delete note by id
// @access Public
router.delete('/:id', (req, res) => {
  Note.findByIdAndRemove(req.params.id, req.body)
    .then(note => res.json({ mgs: 'Note entry deleted successfully' }))
    .catch(err => res.status(404).json({ error: 'No such a note' }));
});

module.exports = router;