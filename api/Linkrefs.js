const express = require('express');
const router = express.Router();

// Load Linkref model
const Linkref = require('../database/models/Linkref');

// @route GET api/linkrefs/test
// @description tests linkrefs route
// @access Public
router.get('/test', (req, res) => res.send('route testing!'));

// @route GET api/linkrefs
// @description Get all linkrefs
// @access Public
router.get('/', (req, res) => {
  Linkref.find()
    .then(linkrefs => res.json(linkrefs))
    .catch(err => res.status(404).json({ nolinkrefsfound: 'No Linkrefs found' }));
});

{/*
// @route GET api/linkrefs/:id
// @description Get single linkref by id
// @access Public
router.get('/:id', (req, res) => {
  Linkref.findById(req.params.id)
    .then(linkref => res.json(linkref))
    .catch(err => res.status(404).json({ nolinkreffound: 'No Linkref found' }));
});
*/}

// @route GET api/linkrefs/:id
// @description Get single linkref by noteid
// @access Public
router.get('/:id', (req, res) => {
  Linkref.findById(req.params.id)
    .then(linkref => res.json(linkref))
    .catch(err => res.status(404).json({ nolinkreffound: 'No Linkref found' }));
});

// @route GET api/linkrefs
// @description add/save linkref
// @access Public
router.post('/', (req, res) => {
  Linkref.create(req.body)
    .then(linkref => res.json({ msg: 'Linkref added successfully' }))
    .catch(err => res.status(400).json({ error: 'Unable to add this linkref' }));
});

// @route GET api/linkrefs/:id
// @description Update linkref
// @access Public
router.put('/:id', (req, res) => {
  Linkref.findByIdAndUpdate(req.params.id, req.body)
    .then(linkref => res.json({ msg: 'Updated successfully' }))
    .catch(err =>
      res.status(400).json({ error: 'Unable to update the Database' })
    );
});

// @route GET api/linkrefs/:id
// @description Delete linkref by id
// @access Public
router.delete('/:id', (req, res) => {
  Linkref.findByIdAndRemove(req.params.id, req.body)
    .then(linkref => res.json({ mgs: 'Linkref entry deleted successfully' }))
    .catch(err => res.status(404).json({ error: 'No such a linkref' }));
});

module.exports = router;