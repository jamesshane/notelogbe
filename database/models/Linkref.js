const mongoose = require('mongoose');

const LinkrefSchema = new mongoose.Schema({
  label: {
    type: String,
    required: true
  },
  noteid: {
    type: String
  },
  linkref: {
    type: String
  },
  updated_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Linkref = mongoose.model('linkref', LinkrefSchema);